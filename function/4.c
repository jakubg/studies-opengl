#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

GLfloat zoom			= -20.0;
GLfloat rotObsY			=  25.0;
GLfloat rotObsX			=  20.0;
int pomocnicze = 1;
float tab[300][200];

typedef struct punkt {
	float x;
	float y;
	float z;
	float r;
	float g;
} Point;

Point points[300*200];

float floatabs(float a)
{
	if(a < 0) return -1.0f*a;
	else return a;
}

float countFunc(float x, float y)
{
	return cos(sqrt(x*x+y*y))*exp(-0.01*(x*x+y*y));
}

// Logic
static void
Logic() {
	float xmin = -10.0f;
	float xmax = 10.0f;
	float ymin = -10.0f;
	float ymax = 10.0f;
	float w = 300;
	float h = 200;

	float xpx = (floatabs(xmin) + floatabs(xmax))/w;
	float ypx = (floatabs(ymin) + floatabs(ymax))/h;
	for(int x=0; x<w; x++)
	{
		for(int y=0; y<h; y++)
		{
			tab[x][y] = countFunc(xmin+xpx*x, ymin+ypx*y);
		}
	}
	
	int i=0;
	float zmin = tab[0][0], zmax = tab[0][0];
	for(int x=0; x<w; x++)
	{
		for(int y=0; y<h; y++)
		{
			if(tab[x][y] < zmin) zmin = tab[x][y];
			if(tab[x][y] > zmax) zmax = tab[x][y];
		}
	}

	for(int x=0; x<w; x++)
	{
		for(int y=0; y<h; y++)
		{
			points[i].x = xmin+xpx*x;
			points[i].y = ymin+ypx*y;
			points[i].z = tab[x][y];
			points[i].r = (tab[x][y] - zmin) / (zmax - zmin);
			points[i].g = 1.0f - points[i].r;
			i++;
		}	
	}
}

void Rysuj(void)
{
	glEnable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);

	if(pomocnicze == 1)
	{
		glBegin(GL_LINES);
	
		glColor3f(1.0, 0.0, 0.0);
		glVertex3f(-10000.0, 0.0, 0.0);
		glVertex3f(10000.0, 0.0, 0.0);
	
		glColor3f(0.0,1.0,0.0);	
		glVertex3f(0.0, -10000.0, 0.0);
		glVertex3f(0.0, 10000.0, 0.0);
	
		glColor3f(0.0,0.0,1.0);	
		glVertex3f(0.0, 0.0, -10000.0);
		glVertex3f(0.0, 0.0, 10000.0);
	
		glEnd();
	}

	glPushMatrix();
	for(int i=0; i<300*200; i++)
	{
		glPushMatrix();
			glBegin(GL_POINTS);
				glColor3f(points[i].r, points[i].g, 0.0f);
				glVertex3f(points[i].x, points[i].z, points[i].y);
			glEnd();
		glPopMatrix();
	}
	glPopMatrix();
}

void WyswietlObraz(void)
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glPushMatrix();

	glTranslatef(0, 0, zoom);
	glRotatef(rotObsX, 1, 0, 0);
	glRotatef(rotObsY,0,1,0);
	Rysuj();
	glPopMatrix();
	glutSwapBuffers();
}

void UstawParametryWidoku(int szerokosc, int wysokosc)
{
	glViewport(0, 0, szerokosc, wysokosc);
	glShadeModel(GL_SMOOTH);
	glClearColor(0, 0, 0, 0);	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(40.0, (float)szerokosc/(float)wysokosc, 1.0, 1000.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void ObslugaKlawiszySpecjalnych(int klawisz, int x, int y)
{
	switch(klawisz)
		{
		case GLUT_KEY_UP:
			rotObsX = rotObsX + 2.0;
			break;

		case GLUT_KEY_DOWN:
			rotObsX = rotObsX - 2.0;
			break;

		case GLUT_KEY_LEFT:
			rotObsY = rotObsY - 2.0;
			break;

		case GLUT_KEY_RIGHT:
			rotObsY = rotObsY + 2.0;
			break;
			
		case GLUT_KEY_PAGE_UP:
			zoom = (zoom <= -1.0) ? zoom + 5.0 : zoom;
			break;

		case GLUT_KEY_PAGE_DOWN:
			zoom = (zoom >= -1000.0) ? zoom - 5.0 : zoom;
			break;
		}
}

void ObslugaKlawiatury(unsigned char klawisz, int x, int y)
{
	switch(klawisz)
	{
		case 'p': pomocnicze = -pomocnicze; break;
		case 27: exit(0); break;
	}
}

int  main(int argc, char **argv)
{
	glutInit(&argc, argv);

	glutInitDisplayMode (GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH);
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(600, 400);
	glutCreateWindow("3D Plot");

	glEnable(GL_DEPTH_TEST);

	glClearDepth(1000.0);
	glClearColor (0.0, 0.0, 0.0, 0.0);

	glutDisplayFunc(WyswietlObraz);
	glutReshapeFunc(UstawParametryWidoku);
	glutIdleFunc(WyswietlObraz);
	glutKeyboardFunc(ObslugaKlawiatury);
	glutSpecialFunc(ObslugaKlawiszySpecjalnych);
	Logic();
	glutMainLoop();

	return 0;
}

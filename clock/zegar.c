/*
	gcc -o robot.exe -Wall robot.c glut32.lib -lopengl32 -lglu32
*/

#include <stdio.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <time.h>

/* Wskazniki do wykorzystywanych kwadryk */
GLUquadricObj *podstawaSciany;
GLUquadricObj *podstawaDyskG;

GLfloat zoom			= -10.0;
GLfloat rotObsY			=  25.0;
GLfloat rotObsX			=  20.0;

/* Funkcja inicjujaca elementy skladowe ramienia robota zamodelowane jako kwadryki */
void InicjujRamieRobota(void)
{
	/* Zainicjowanie scian bocznych walca bedacego podstawa ramienia */
	podstawaSciany = gluNewQuadric();
	gluQuadricDrawStyle(podstawaSciany, GLU_LINE);
	
	// Zainicjowanie gornej podstawy walca
	podstawaDyskG = gluNewQuadric();
	gluQuadricDrawStyle(podstawaDyskG, GLU_LINE);
}


/*	Funkcja rysujaca obraz sceny widzianej z biezacej pozycji obserwatora
	Zalozenie: Funkcja glutWireSpehere moze ryswac tylko sfere o promieniu 1 */
void RysujRamieRobota()
{
	struct tm *now = NULL;
	time_t time_value = 0;
	time_value = time(NULL);
	now = localtime(&time_value);
	int hour = now->tm_hour;
	if(hour>12) hour -= 12;
	
	int minAngle = 6 * now->tm_min;
	int hourAngle = 30 * hour + (30 * ((float)minAngle / 360.0));
	int secAngle = 6 * now->tm_sec;
	
	printf("%d:%d\n", now->tm_sec, secAngle);
/*
   //Linie pomocnicze 
	glBegin(GL_LINES);
	
	glColor3f(1.0, 0.0, 0.0);		// Os X
	glVertex3f(-30.0, 0.0, 0.0);
	glVertex3f(30.0, 0.0, 0.0);
	
	glColor3f(0.0,1.0,0.0);			// Os Y
	glVertex3f(0.0, -5.0, 0.0);
	glVertex3f(0.0, 30.0, 0.0);
	
	glColor3f(0.0,0.0,1.0);			// Os Z
	glVertex3f(0.0, 0.0, -30.0);
	glVertex3f(0.0, 0.0, 30.0);
	
	glEnd();
*/
/* Ustalamy kolor bialy */
	glColor3f(1.0,1.0,1.0);

/* Rysowanie tarczy zegara */
	glPushMatrix();
	glColor4f(1.0,1.0,1.0,99.1);
	gluCylinder(podstawaSciany, 2.85, 3.0, 0.4, 12.0, 1);
	gluDisk(podstawaDyskG, 0.0, 2.85, 12, 1);
	//glutSolidCube(1);
	//gluDisk(podstawaDyskG, 0.0, 2.85, 60, 1);
	glPopMatrix();

	glPushMatrix();
	glColor3f(1.05,0.0,0.0);
	glRotatef(-hourAngle, 0, 0, 1);
	glRotatef(90, 0, 0, 1);
	glScalef(1.5, 0.05, 0.05);
	glTranslatef(0.5, 0.0, 0.0);
	//glutWireCube(1);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glColor3f(0.1,1.2,0.1);
	glRotatef(-minAngle, 0, 0, 1);
	glRotatef(90, 0, 0, 1);
	glScalef(1.9, 0.05, 0.05);
	glTranslatef(0.5, 0.0, 0.0);
	//glutWireCube(1);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glColor3f(0.0,0.0,1.05);
	glRotatef(-secAngle, 0, 0, 1);
	glRotatef(90, 0, 0, 1);
	glScalef(2.3, 0.05, 0.05);
	glTranslatef(0.4, 0.0, 0.0);
	//glutWireCube(1);
	glutSolidCube(1);
	glPopMatrix();

/* Sprzatamy */
	glPopMatrix();
}

/* Funkcja generujaca pojedyncza klatke animacji */
void WyswietlObraz(void)
{
	/* Wyczyszczenie bufora ramki i bufora glebokosci */
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	
	/* Powielenie macierzy na wierzcholku stosu */
	glPushMatrix();
	
	/* Wyznaczenie polozenia obserwatora (przeksztalcenie uladu wspolrzednych, sceny do ukladu wspolrzednych obserwatora). */
	glTranslatef(0, 0, zoom);
	glRotatef(rotObsX, 1, 0, 0);
	glRotatef(rotObsY,0,1,0);
	
	/* Generacja obrazu sceny w niewidocznym buforze ramki */
	RysujRamieRobota();

	/* Usuniecie macierzy lezacej na  wierzcholku stosu (powrot do stanu sprzed wywolania funkcji) */
	glPopMatrix();

	/* Przelaczenie buforow ramki */
	glutSwapBuffers();
}

/* Funkcja ustawiajaca parametry rzutu perspektywicznego i rozmiary viewportu */
void UstawParametryWidoku(int szerokosc, int wysokosc)
{
	/* Ustawienie parametrow viewportu */
	glViewport(0, 0, szerokosc, wysokosc);

	/* Przejscie w tryb modyfikacji macierzy rzutowania */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(40.0, (float)szerokosc/(float)wysokosc, 1.0, 1000.0);

	/* Przejscie w tryb modyfikacji macierzy przeksztalcen geometrycznych */
	glMatrixMode(GL_MODELVIEW);

	/* Zmiana macierzy znajdujacej sie na wierzcholku stosu na macierz jednostkowa */
	glLoadIdentity();
}

/* Funkcja klawiszy specjalnych */
void ObslugaKlawiszySpecjalnych(int klawisz, int x, int y)
{
	switch(klawisz)
		{
		case GLUT_KEY_UP:
			//rotObsX = (rotObsX < 90.0) ? rotObsX + 1.0 : rotObsX;
			rotObsX = rotObsX + 2.0;
			break;

		case GLUT_KEY_DOWN:
			//rotObsX = (rotObsX > 0.0) ? rotObsX - 1.0 : rotObsX;
			rotObsX = rotObsX - 2.0;
			break;

		case GLUT_KEY_LEFT:
			//rotObsY = (rotObsY > -180.0) ? rotObsY - 1.0 : rotObsY;
			rotObsY = rotObsY - 2.0;
			break;

		case GLUT_KEY_RIGHT:
			//rotObsY = (rotObsY < 180.0) ? rotObsY + 1.0 : rotObsY;
			rotObsY = rotObsY + 2.0;
			break;
			
		case GLUT_KEY_PAGE_UP:
			zoom = (zoom <= -10.0) ? zoom + 5.0 : zoom;
			break;

		case GLUT_KEY_PAGE_DOWN:
			zoom = (zoom >= -200.0) ? zoom - 5.0 : zoom;
			break;
		}
}

/* Funkcja obslugi klawiatury */
void ObslugaKlawiatury(unsigned char klawisz, int x, int y)
{
	
	switch(klawisz)
		{
		}

	if(klawisz == 27)
			exit(0);
}

/* Glowna funkcja programu */
int  main(int argc, char **argv)
{
	
	/* Zainicjowanie biblioteki GLUT */
	glutInit(&argc, argv);

	/* Ustawienie trybu wyswietlania */
	glutInitDisplayMode (GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH);

	/* Ustawienie polozenia dolenego lewego rogu okna */
	glutInitWindowPosition(100, 100);

	/* Ustawienie rozmiarow okna */
	glutInitWindowSize(600, 400);

	/* Utworzenie okna */
	glutCreateWindow("Arm");

	/* Odblokowanie bufora glebokosci */
	glEnable(GL_DEPTH_TEST);

	/* Ustawienie funkcji wykonywanej na danych w buforze glebokosci */
	glDepthFunc(GL_LEQUAL);

	/* Ustawienie wartosci czyszczacej zawartosc bufora glebokosci */
	glClearDepth(1000.0);

	/* Ustawienie koloru czyszczenia bufora ramki */
	glClearColor (0.0, 0.0, 0.0, 0.0);

	/* Zarejestrowanie funkcji (callback) wyswietlajacej */
	glutDisplayFunc(WyswietlObraz);

	/* Zarejestrowanie funkcji (callback) wywolywanej za kazdym razem kiedy zmieniane sa rozmiary okna */
	glutReshapeFunc(UstawParametryWidoku);

	/* Zarejestrowanie funkcji wykonywanej gdy okno nie obsluguje zadnych zadan */
	glutIdleFunc(WyswietlObraz);

	/* Zarejestrowanie funkcji obslugi klawiatury */
	glutKeyboardFunc(ObslugaKlawiatury);

	/* Zarejestrowanie funkcji obslugi klawiszy specjalnych */
	glutSpecialFunc(ObslugaKlawiszySpecjalnych);

	/* Zainicjowanie kwadryk tworzacych ramie robota */
	InicjujRamieRobota();

	/* Obsluga glownej petli programu (wywolywanie zarejestrowanych callbackow w odpowiedzi na odbierane zdarzenia lub obsluga stanu bezczynnosci) */
	glutMainLoop();

	return 0;
}